package com.owc.hbase.filter;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.exceptions.DeserializationException;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.FilterBase;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * Pass results whose specified part is between the given ranges. Every range is determined by
 * offset, length, start and end values.
 *
 * @author Sebastian Land
 *
 */
public class PartRangeRowKeyFilter extends FilterBase {

    protected byte[] partStartValue = null;
    protected byte[] partEndValue = null;
    protected int offset;

    protected boolean filterRow = true;

    public PartRangeRowKeyFilter(int offset, byte[] startValue, byte[] endValue) {
        partStartValue = startValue;
        partEndValue = endValue;
        this.offset = offset;
    }

    @Override
    public boolean filterRowKey(Cell cell) {
        byte[] buffer = cell.getRowArray();
        int offset = cell.getRowOffset();
        int length = cell.getRowLength();
        if (buffer == null || partStartValue == null) {
            return true;
        }
        if (length < partStartValue.length) {
            return true;
        }

        // if buffer part is larger or equal to start and smaller to end , return false => pass row
        // else return true, filter row
        int cmp = Bytes.compareTo(buffer, offset + this.offset, partStartValue.length, partStartValue, 0, partStartValue.length);
        if (cmp < 0) {
            filterRow = true;
        } else {
            cmp = Bytes.compareTo(buffer, offset + this.offset, partEndValue.length, partEndValue, 0, partEndValue.length);
            filterRow = (cmp >= 0);
        }
        return filterRow;
    }

    @Override
    public ReturnCode filterCell(Cell v) {
        if (filterRow)
            return ReturnCode.NEXT_ROW;
        return ReturnCode.INCLUDE;
    }

    // Override here explicitly as the method in super class FilterBase might do a KeyValue
    // recreate.
    // See HBASE-12068
    @Override
    public Cell transformCell(Cell v) {
        return v;
    }

    @Override
    public boolean filterRow() {
        return filterRow;
    }

    @Override
    public byte[] toByteArray() {
        try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream(12 + partStartValue.length + partEndValue.length)) {
            DataOutputStream out = new DataOutputStream(byteOut);
            out.writeInt(offset);
            out.writeInt(partStartValue.length);
            out.writeInt(partEndValue.length);
            out.write(partStartValue);
            out.write(partEndValue);
            out.flush();
            return byteOut.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException("Error when serializing the PartRangeRowKeyFilter.", e);
        }
    }

    // @Override
    public static Filter parseFrom(final byte[] pbBytes) throws DeserializationException {
        try (DataInputStream in = new DataInputStream(new ByteArrayInputStream(pbBytes))) {
            int offset = in.readInt();
            int startLength = in.readInt();
            int endLength = in.readInt();
            byte[] start = new byte[startLength];
            byte[] end = new byte[endLength];
            in.readFully(start);
            in.readFully(end);

            return new PartRangeRowKeyFilter(offset, start, end);
        } catch (IOException e) {
            throw new RuntimeException("Error when deserializing PartRangeRowKeyFilter", e);
        }
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " between " + Bytes.toStringBinary(partStartValue) + " and " + Bytes.toStringBinary(partEndValue);
    }
}
